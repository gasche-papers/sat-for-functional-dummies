\documentclass[a4paper]{easychair}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\title{Conflicts in logic: SAT for dummies}
%\thanks{thanks}

\author{
  Gabriel Scherer\inst{1}
\and
  Stéphane Graham-Lengrand\inst{1}\inst{2}
}
\institute{
  INRIA Saclay — Île-de-France \& LIX/École polytechnique,
  Palaiseau, France\\
\and
  CNRS
}

\authorrunning{Scherer and Graham-Lengrand}
\titlerunning{Conflicts in logic: SAT for dummies}

\usepackage{natbib}
\usepackage{epigraph}

\newcommand{\UNSAT}{\mathsf{UNSAT}}
\newcommand{\SAT}{\mathsf{SAT}}

\newcommand{\bool}{b}
\newcommand{\true}{\top}
\newcommand{\false}{\bot}

\newcommand{\subst}[2]{[{#1} := {#2}]}

\newcommand{\seq}[2]{#1 \vdash #2}

\usepackage{authoredcomments}
%% Use \Xgabriel{comment} or \Xstephane{comment}
%% or the full form
%% \Xgabriel[text that is being commented]{comment}

%% Uncomment the following line to hide the comments
%\UNXXX

\usepackage{todos}
%% use \TODO{...} for a TODO item

%% Uncomment the following line to hide the TODO items
%\UNTODO

\begin{document}

\maketitle

\begin{abstract}
  The SAT-solving community thinks of their work at the level of
  search algorithms. Constructing proofs is an afterthought, once
  search has found a contradiction. In contrast, those of us more
  familiar with type and proof theory will often think of the proofs
  first; seeing those search algorithms in terms of progressive proof
  construction helps us understand and reuse them. The aim of the
  present work is to introduce modern SAT search algorithms to
  logicians, presenting model search as progressive proof construction.

  The obvious propagation+backtracking algorithm to solve boolean SAT
  queries is called DPLL. The proof-construction presentation of DPLL
  is fairly clear. But the main reason why modern SAT solvers work
  well is the addition of conflict-driven clause learning (CDCL), and
  its meaning for proofs is less clear. We propose to understand
  conflict-clause learning in terms of relevant simplification of
  sub-derivations: the natural operation of removing useless formulas
  from the context and goal of a judgment.
\end{abstract}

\Xgabriel{I am not sure yet which level of formalism is best for the
  paper. The current plan below is very precise: we show the
  rewrite-system modelling used in SAT papers and establish the formal
  correspondence between its states and the leaves of the partial
  proof. Maybe we could be much less formal than that, focus on
  informally explaining the main point about conflict resolution, and
  not prove the correspondence.

  Being less formal would give a shorter paper, possibly more
  accessible. But maybe it is possible of being precise and accessible
  at the same time, by making the notations light and not spending
  much time on the formal part?}

\section{Introduction}

The SAT community has developed strategies that prove very effective
for exploring large search spaces. Not only have SAT solvers
themselves (and variants such as MaxSAT) plenty of practical
applications, many of the state-of-the-art solvers for more
expressive/interesting logics integrate a SAT solver at their core
(this is the basis for SMT (Sat Modulo Theory) solvers such as Z3
or CVC4), or at least integrate the ideas that have proved succesful
for SAT solving \citep*{TODO:Leviathan,
  TODO:the-paper-stephane-mentioned-about-intuitinoistic-proof-search}. If
you are interested in some sort of proof search for some logic or type
theory, having an idea of how SAT solvers work may be of use someday.

The first author of the present work, the prototypical ``SAT dummy'',
did not know anything about SAT solvers a month before starting this
paper. They started asking questions to the second author, who has
worked on SAT solving and its proof-theoretic presentation, but had to
admit that some basic intuitions have not been written up in detail.

\epigraph{If there is a book that you want to read, but it hasn't been
  written yet, then you must write it.}{Toni Morrison}

\subsection{Related Work}

Conflict-clause learning can be explained and justified
(in particular, proved correct) in terms of \emph{resolution}
\citep*{TODO} -- a proof system for classical logic. Instead, we
represent our proof derivations in a classical sequent
calculus. Sequent calculus is a proof representation framework that is
not specific to classical logic, so hopefully the intuitions we
provide can transfer to ideas about proof search in other logics, such
as intuitionistic or linear logics. It is also closer to the type
theories that type-system designers are working with, making our
presentation accessible to a different audience.

There has been a lot of work in the last few years, within the SAT
community, on the question of getting SAT solvers to provide proof
evidence that can be re-checked after the fact \citep*{TODO,
  TODO...} -- choosing evidence formats that can be emitted without
performance penalty, and re-checked with a good compromise between
size of the evidence and checking time. Our purpose is purely
didactic.

Finally, \citet*{dpllt-focusing} gives a logical interpretation of the
DPLL(T) search algorithm (a naive Sat-Modulo-Theory algorithm). The
focus of their work is to propose a well-chosen logic (using the
logical technique of \emph{focusing}) such that the ``natural'' way to
do proof search of this logic mimicks the DPLL search algorithm. This
is a nice way to show how to reconstruct DPLL using the toolbox of
proof theory, but it results in a logic that is more complex than what
a wide audience would be familiar to. We use the standard sequent
calculus for classical propositional logic, whose structure does not
suggest any particular search strategy (in particular not the SAT
algorithms we are studying), but still let us represent the
\emph{proofs} produced by the SAT search process. (We could use the
calculus of \citet*{dpllt-focusing} or an extension of it, and get
a stronger correspondence, but the accessibility of the presentation
may suffer.)

\subsection{SAT basics}

Let us first recall a few basic definitions. A \emph{literal} $l$ is
either a boolean variable $x_i$ or its negation $\neg{x_i}$; we write
$l$ for literals. A \emph{clause} $C$ is a disjunction of literals and
\emph{clause} a disjunction of literals
$l_1 \vee l_2 \vee \dots \vee l_n$. We only consider \emph{formulas}
$F$ in conjunction normal form (CNF), that is a conjunction of
disjunction of literals, that is is a conjunction of clauses
$C_1 \wedge C_2 \wedge \dots \wedge C_m$.

The SAT-solving algorithms that we study take a formula $F$, and
return either $\UNSAT$, if the formula is unsatisfiable, or
$\SAT(x_i \mapsto \bool_i)^{i \in I}$, if the formula is satisfiable
with the assignment of each variable $x_i$ to a boolean constant
$\bool_i$, that is if the formula
$F{\subst {x_i} {\bool_i}}^{i \in I}$ is true. (We write $\true$ and
$\false$ for the boolean constants.)

Note that the problem is decidable (there are only $2^n$ cases to
consider for a problem of $n$ variables), so can always eventually get
one of those two answers -- the search algorithms we discuss are
complete, they always find the answer if its exists.  More general
satisfiability problems for more expressive logics are undecidable, so
their algorithms cannot be complete and may non-terminate or give up
without giving an answer. Even for SAT, implementations do in fact
time out on big formulas.

\subsection{Sequent calculus basics}

\TODO{Recall the sequent rules that we will be using for classical
  logic, in particular the cut rule (it shouldn't be restricted to cut
  on atoms, as clause learning will cut on full clauses). Remark that
  ``boolean variables'' are \emph{atomic types}, not term variables of
  boolean type. From the point of view of type-system people we are
  using *second-order* quantifiers for universals and existentials.}

\subsection{What are we proving?}

\TODO{We could say that the search algorithm is attempting to build
  proofs of both $\exists (x_i)^i, F$ and of $\forall (x_i)^i, \neg F$
  simultaenously. If we only focus on one of the two sides, then we
  need to explain how, when search for the proof fails, we are able to
  build a proof of the negation.

  Thus it is more natural to emphasize $\forall (x_i)^i, \neg F$: if
  the search fails in a leaf of the search tree, then it is immediate
  to check that we have a proof of the negation
  $\exists (x_i)^i, F$. On the contrary, if we presented the search as
  a search for a proof of $\exists (x_i)^i, F$, then it would not be
  clear at all to see why a failure, once the search space is
  exhausted, should let us construct a proof of
  $\forall (x_i)^i, \neg F$.

  So we are looking for a sequent proof of
  $\seq {} {\forall (x_i)^i, \neg F$}, that is
  \[ \seq F \false \]
 }

\section{Trail, propagation, and proof construction}

\TODO{Explain the basic DPLL algorithm, use it to introduce the trail
  and propagation. Show the rewrite-system presentation. Then show the
  corresponding proof construction; decision is a cut with two search
  premises, propagation is a series of disjunction eliminations
  occurring whenever a variable is added to the context (all clauses
  that contained the variable are eliminated to remove this variable
  from the clause; when a clause becomes a single variable, it
  propagates in turn).

  Correspondence between the rewrite system and the partial proof
  (repeated from \citet*{dpllt-focusing}): the decision marks in the
  trail corresponds to open leaves, and the trail (decision marks +
  propagation marks) explains what propagations where done, so we can
  compute the proof context from it (for each leaf).}

\section{Conflicts and clause learning}

\TODO{Explain conflicts, the implication graph, the unique implication
  point, clause learning. Show the proof-side view: conflict
  resolution corresponds to constructing a minimal/relevant
  derivation. Clause learning consists of cutting on the conflict
  clause.}

\section{Levels and backjumping}

\TODO{Explain levels and backjumping / non-chronological
  backtracking -- going back to the second youngest level on
  conflict. There is no enlightening proof-side view, but we should
  still point out that it is easy to determine levels by looking at
  the proof shape (each decision cut determines a level), and that
  backjumping is only the idea of extruding the cut as close to the
  root as syntactically possible.

  (Would a proof term representation help? We can represent full
  (non-analytic) cuts as a let-binding, and backjumping as
  let-extrusion. But maybe that only makes the framework heavier.)}

\section{Clause activity}

\TODO{Explain clause activity heuristic, and point out that with our
  view of conflict resolution as trimming it can be computed by
  traversing the partial proof and increasing activity on each atom
  occurrence, decaying on each cut. In what sense does this
  correspond to distance within the proof tree?}

\section{Conclusion}

\TODO{SAT conflict resolution is ``just'' building a relevant/minimal
  proof derivation.}

% \subsection*{Acknowledegments}

\newpage

\bibliographystyle{plainnat}
\bibliography{sat}

\end{document}